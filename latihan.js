
// console.log("ini baris pertama")
// console.log("ini baris kedua")
// console.log("ini baris ketiga")

// setTimeout(() => console.log("ini baris pertama"), 3000)
// setTimeout(() => console.log("ini baris kedua"), 1000)
// setTimeout(() => console.log("ini baris ketiga"), 2000)


// const posts1 = [
//     {
//       title: "Post one1",
//       body: "This is post one"
//     },
//     {
//       title: "Post two",
//       body: "This is post two"
//     }
//   ]
  
// const createPost1 = post => {setTimeout(() => {posts1.push(post)}, 2000)}

// const getPosts1 = () => {setTimeout(() => {posts1.forEach(post => {console.log(post)})}, 3000)}

// const newPost1 = {title: "Post three", body: "This is post three"}
  
// //ada Call Back sehingga Post Three  keluar karena di pnggil lagi
// createPost1(newPost1, getPosts1) 
// getPosts1()

  //Tidak ada Call back sehingga Post Tree tidak keluar karena kalah cepat
//   createPost(newPost)
//     getPosts()


// //==========================================================
//   const posts = [
//     {
//       title:"Post one",
//       body:"This is post one"
//     },
//     {
//       title:"Post two",
//       body:"This is post two"
//      }
//   ]
  
//   const createPost = post => {
//      return new Promise((resolve, reject) => {
//        setTimeout(() => {
//          posts.push(post)
//          const error=false
//          if(!error) {
//            resolve()
//          }else{
//            reject()
//          }
//        }, 2000)
//      })
//   }
  
//   const getPosts = () => {
//       setTimeout(() => {
//         posts.forEach(post => {
//          console.log(post)
//         })
//       }, 1000)
//   }
  
//   const newPost = {
//     title:"Post three",
//      body:"This is post three"
//   }
  
//   createPost(newPost)
//      .then(getPosts)
//      .catch(error => console.log(error))
   
const posts = [
    {
       title:"Post one",
       body:"This is post one"
    },
    {
       title:"Post two",
       body:"This is post two"
    }
 ]
 
 const createPost = post => {
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
           posts.push(post)
           const error = false
           if(!error) {
              resolve()
           }else{
              reject()
           }
        }, 2000)
      })
 }
 
 const getPosts = () => {
      setTimeout(() => {
         posts.forEach(post => {
            console.log(post)
          })
      }, 1000)
 }
 
 const newPost = {
    title:"Post three",
    body: "This is post three"
 }
 
 const init = async() => { 
      await createPost(newPost)
      getPosts()
 }
 
 init()